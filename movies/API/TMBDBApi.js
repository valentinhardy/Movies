const API_TOKEN = "464cc19789644f74ae9c25c1757020eb";

export function getFilmsFromApiWithSearchedText (text, page) {

    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text + '&page=' + page 

    //appel api
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.error(error))
    
}

export function getImageFromApi(name){ //retourne le lien choisi + name = film.poster_path dans l'API
    return 'https://image.tmdb.org/t/p/w300'+ name
}