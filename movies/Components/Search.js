//on importe les components
import React from 'react'
import FilmItem from './FilmItem'
import { getFilmsFromApiWithSearchedText } from '../API/TMBDBApi'
import { ActivityIndicator, View, TextInput, Button, StyleSheet, FlatList, Text } from 'react-native'

class Search extends React.Component {

    constructor(props) {
        super(props)
        this.page = 0,
        this.totalPages = 0,
        this.searchedText = ''
        this.state = { 
            films: [],
            isLoading: false //par defaut
        }
    }
    
    //averti du changement de text // maj du texte
    _searchTextInputChanged(text){
       this.searchedText = text
    }

    //affiche la vue chargement
    _displayLoading() {
        if (this.state.isLoading) { // = true
            return (
                <View style={styles.loading_container}>
                <ActivityIndicator size='large' />
                </View>
            )
        }
    }
    //functions charge les films
    _loadFilms() {
        console.log(this.searchedText) // Un log pour vérifier qu'on a bien le texte du TextInput
        if (this.searchedText.length > 0) {
            this.setState({isLoading: true}) // lancement du chargement
            getFilmsFromApiWithSearchedText(this.searchedText, this.page+1).then(data => {
                this.page = data.page
                this.totalPages = data.total_pages
                this.setState({ 
                    films: [...this.state.films, ...data.results],
                    isLoading: false // arret du chargement
                }) 
            })
        }
    }
    _searchFilms(){
        this.page = 0
        this.totalPages = 0
        this.setState({
            films: []
        }, () => {
            console.log('Page : ' + this.page + '/ TotalPages : ' + this.totalPages + '/ Nombre de films :' + this.state.films.length),
            this._loadFilms()
        })
    }

    _displayDetailsForFilms = (idFilm) => {
        this.props.navigation.navigate('FilmDetails', { idFilm: idFilm })
    }

    //rendu
    render(){
        console.log(this.state.isLoading)
        return (
            <View style={styles.main_container}>
                <View style={styles.form_container}>
                    <TextInput 
                    style={styles.textinput} 
                    placeholder='Titre du film'
                    //changement de texte
                    onChangeText={(text) => this._searchTextInputChanged(text)} 
                    //charge les films par le bouton retour clavier
                    onSubmitEditing={() => this._searchFilms()}
                    />
                    <Button color="#841584" title='Rechercher' onPress={() => this._searchFilms()}/>
                </View>
                <FlatList
                    data={this.state.films}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <FilmItem film={item} displayDetailsForFilms={this._displayDetailsForFilms}/>}
                    onEndReachedThreshold={2}
                    onEndReached={() => {
                        if(this.page < this.totalPages ){
                            this._loadFilms()
                        }
                    }}
                />
                {this._displayLoading()}
            </View>
        )
    }    
}


const styles = StyleSheet.create ({ 
    form_container: {
        padding: 10,
    },
    main_container: {
        flex: 1,
    },
    textinput: {
        marginLeft: 5,
        marginRight: 5, 
        height: 50, 
        borderRadius: 5,
        borderColor: '#845EC2',
        borderWidth: 1,
        paddingLeft: 15
    },
    loading_container: {
        position: 'absolute',
        backgroundColor: '#rgba(0,0,0,0.2)',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
      }

});


//export du fichier Search.js
export default Search